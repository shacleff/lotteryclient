cc.Class({
    extends: cc.Component,

    properties: {
        pfOpenItem:{
            default:null,
            type:cc.Prefab
        },

        //基本走势普通
        pfBaseItem:{
            default:null,
            type:cc.Prefab
        },

        //走势统计
        pfBase1Item:{
            default:null,
            type:cc.Prefab
        },

        //形态走势普通
        pfFormItem:{
            default:null,
            type:cc.Prefab
        },
        //形态统计
        pfForm1Item:{
            default:null,
            type:cc.Prefab
        },

        //开奖界面
        ndOpenPanle:{
            default:null,
            type:cc.Node
        },

        //走势界面
        ndBasePanle:{
            default:null,
            type:cc.Node
        },

        //形态界面
        ndFormContent:{
            default:null,
            type:cc.Node
        },

        spSort:{
            default:[],
            type:cc.SpriteFrame
        },

        _data:null,
        _reaCount: 18,
        _spacing: 4,
        _totalCount: 50,
        _openItemArr: [],
        _addType: false,
        _addType1: false
    },

    // use this for initialization
    onLoad: function () {
        var self = this;
        this.showPage(this._data);
    },

    init:function(data){
        this._data = data;
    },

    showPage:function(info){
        this._color = [];
        var color = [];
        var color1 = new cc.Color(253, 253, 251);
        var color2 = new cc.Color(246, 247, 241);
        color.push(color1);
        color.push(color2);
        this._color.push(color1);
        this._color.push(color2);

        var tempinfo = eval('('+ info +')');
        var trdata = tempinfo["tr"]; 
        
        this._openData = trdata;
        this._totalCount =this._openData.length;
        this.lastPositionY =0;
        this.buffer =730;

        for(var i=0;i<this._reaCount;++i){
            var openItem =cc.instantiate(this.pfOpenItem);
            openItem.setPosition(0,-openItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                form:this._openData[i].x,
                color:this._color[i%2]
            };
            openItem.getComponent(openItem.name).onItemIndex(i);
            openItem.getComponent(openItem.name).init(data); 
            this.ndOpenPanle.addChild(openItem);
            this._openItemArr.push(openItem);
        }
        this.openItemHeight =openItem.height;
        this.ndOpenPanle.height =this._totalCount*(this.openItemHeight+this._spacing)+this._spacing;

        this.baseItemArr=[];
        this.formItemArr =[];
        this.base1ItemArr =[];
        this.form1ItemArr =[];
        for(var i=0;i<this._openData.length;i++)
        {
            //基本走势
            var baseItem = cc.instantiate(this.pfBaseItem);
            var data = {
                Isuse:this._openData[i].i,
                num1:this._openData[i].n,
                num2:this._openData[i].s,
                num3:this._openData[i].sp,
                num4:this._openData[i].t,
                color:color[i%2]
            };
            baseItem.getComponent(baseItem.name).init(data);
            this.baseItemArr.push(baseItem);

            //形态走势
            var formItem = cc.instantiate(this.pfFormItem);
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                nums:this._openData[i].z,
                color:color[i%2]
            };
            formItem.getComponent(formItem.name).init(data);
            this.formItemArr.push(formItem);   
        }

        //统计出现期数、最大、平均遗漏、最大连出
        var censusStr = ["出现次数","平均遗漏","最大遗漏","最大连出"];
        var censtrCor1 = new cc.Color(122,30,150);
        var censtrCor2 = new cc.Color(37,87,0);
        var censtrCor3 = new cc.Color(110,35,0);
        var censtrCor4 = new cc.Color(0,96,132);
        var censtrCor5 = new cc.Color(229,225,214);
        var censtrCor6 = new cc.Color(234,233,229);
        var colorBg = [censtrCor5,censtrCor6];
        var colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];

        var bnData = tempinfo["bn"];
        var baData = tempinfo["ba"];
        var bmData = tempinfo["bm"];
        var bsData = tempinfo["bs"];
        var bnamsList = [];
        bnamsList.push(bnData);
        bnamsList.push(baData);
        bnamsList.push(bmData);
        bnamsList.push(bsData);

        var snData = tempinfo["sn"];
        var saData = tempinfo["sa"];
        var smData = tempinfo["sm"];
        var ssData = tempinfo["ss"];
        var snamsList = [];
        snamsList.push(snData);
        snamsList.push(saData);
        snamsList.push(smData);
        snamsList.push(ssData);


        for(var i=0;i<4;i++)
        {
            //基本走势统计
            var base1Item = cc.instantiate(this.pfBase1Item);
            var data = {
                color:color[(this._openData.length+i-1)%2],
                dec:censusStr[i],
                decColor:colorStr[i],
                decBgColor:colorBg[i%2],
                nums:bnamsList[i],
                numColor:colorStr[i]
            }
            base1Item.getComponent(base1Item.name).init(data);
            this.base1ItemArr.push(base1Item);
         
            //形态走势统计
            var form1Item = cc.instantiate(this.pfForm1Item);
            var data = {
                color:color[(this._openData.length+i-1)%2],
                dec:censusStr[i],
                decColor:colorStr[i],
                decBgColor:colorBg[i%2],
                nums:snamsList[i],
                numColor:colorStr[i]
            }
            form1Item.getComponent(form1Item.name).init(data);
            this.form1ItemArr.push(form1Item);
        }
    },

    //开奖期号排序
    issueSort:function(toggle){
        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        if(toggle.getComponent(cc.Toggle).isChecked){
            this._openData =this._openData.reverse(); 
        }
        else{
            this._openData =this._openData.reverse();
        }
       
        for(var i=0;i<this._openItemArr.length;++i){
            this._openItemArr[i].setPosition(0,- this._openItemArr[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                form:this._openData[i].x,
                color:this._color[i%2]
            };
            this._openItemArr[i].getComponent(this._openItemArr[i].name).onItemIndex(i);
            this._openItemArr[i].getComponent(this._openItemArr[i].name).updateData(data); 
        }
    },

    onClose:function(){

    },

    scrollCallBackFun: function(){
        if(!this._openItemArr.length){
            return;
        }else{
            Utils.preNodeComplex(this.ndOpenPanle,this.openItemHeight,this._spacing,this._reaCount,this._openItemArr,this.buffer,this._openData,this._color,'k3_opencontent_item'); 
        }
    },

    //----切换到那个单选按钮，才将它们addChild到舞台上来
    //基本走势
    tgBaseTrend: function(){
        if(this._addType){return}
        for(var i=0;i<this.baseItemArr.length;++i){
            this.ndBasePanle.addChild(this.baseItemArr[i]);  
            this._addType =true; 
        }
        for(var i=0;i<this.base1ItemArr.length;++i){
            this.ndBasePanle.addChild(this.base1ItemArr[i]);
            this._addType =true;    
        }    
    },

    //号码分布
    tgFormTrend: function(){
        if(this._addType1){return}
        for(var i=0;i<this.formItemArr.length;++i){
            this.ndFormContent.addChild(this.formItemArr[i]);  
            this._addType1 =true; 
        }
        for(var i=0;i<this.form1ItemArr.length;++i){
            this.ndFormContent.addChild(this.form1ItemArr[i]);
            this._addType1 =true;    
        }    
    }

});
