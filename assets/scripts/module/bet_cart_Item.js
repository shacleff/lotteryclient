/**
 * !#zh 号码栏组件
 * @information 投注号码，投注方式，注数，倍数，所需钱数
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labNumber:{
           default: null,
           type: cc.Label 
        },
        
        labInfo:{
           default: null,
           type: cc.Label 
        }
       
    },

    // use this for initialization
    onLoad: function () {

    },

    /** 
    * 接收号码栏信息并初始化显示
    * @method init
    * @param {String,Object} num,ninfo
    */
    init: function(num,ninfo){
        this.labNumber.string = num;
        this.labInfo.string = ninfo[0] + " " + ninfo[1] + "注 " + ninfo[2]+"倍 " + ninfo[3]+"元 ";

    },

});
